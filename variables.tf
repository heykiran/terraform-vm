variable "env" {
  type        = string
  description = "environment name"
}

variable "name" {
  type        = string
  description = "name"
}

variable "sa_email" {
  type        = string
  description = "Service account email"
}