resource "google_compute_instance" "vm" {
  name                      = "${var.name}-vm-${var.env}"
  machine_type              = "e2-micro"
  zone                      = "us-east1-b"
  allow_stopping_for_update = true
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  network_interface {
    network = "default"
    access_config {
      // Include this section to give the VM an external ip address
    }
  }
  service_account {
    email  = var.sa_email
    scopes = ["cloud-platform", "storage-full"]
  }
  scheduling {
    preemptible       = true
    automatic_restart = false
  }
}